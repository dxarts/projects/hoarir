HoaDistRIR {
	// mono input
	*ar { arg in, partConvBuffers, fftsize, rho, theta, phi, order = AtkHoa.defaultOrder;
		var numExtraChannels;
		numExtraChannels = HoaOrder.new(order).size - HoaOrder.new(1).size;
		rho = rho.clip(1, 13);
		in = partConvBuffers.collect{ |bufs|
			bufs.collect{ |buffer|
				PartConv.ar(in, fftsize, buffer)
			}
		};
		// select which radius of convolution
		in = SelectX.ar(rho - 1, in);
		// encode from FuMa to Atk
		in = HoaEncodeMatrix.ar(in, HoaMatrixEncoder.newFormat('fuma', 1));
		// tumble to phi
		in = HoaTumble.ar(in, phi, 1);
		// rotate to theta
		in = HoaRotate.ar(in, theta, 1);
		// concatenate with silence to add to a certain order
		in = in ++ Silent.ar(numExtraChannels);
		^in
	}
}

HoaDistRIR3 {
	// mono input, assumes 3rd order IRs
	*ar { arg in, partConvBuffers, fftsize, rho, theta, phi, order = AtkHoa.defaultOrder;
		rho = rho.clip(1, 13);
		in = partConvBuffers.collect{ |bufs|
			bufs.collect{ |buffer, i|
				PartConv.ar(in, fftsize, buffer)
			}
		};
		// select which radius of convolution
		in = SelectX.ar(rho - 1, in);
		// tumble to phi
		in = HoaTumble.ar(in, phi, order);
		// rotate to theta
		in = HoaRotate.ar(in, theta, order);
		^in
	}
}

HoaDistRIRSingle3 {
	// mono input, assumes 3rd order IRs
	*ar { arg in, partConvBuffers, fftsize, theta, phi, order = AtkHoa.defaultOrder;

		in = partConvBuffers.collect{ |buffer|

			PartConv.ar(in, fftsize, buffer)

		};
		// tumble to phi
		in = HoaTumble.ar(in, phi, order);
		// rotate to theta
		in = HoaRotate.ar(in, theta, order);
		^in
	}
}