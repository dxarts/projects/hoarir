HoaRIR {
	var <type, <fftSize, reflection, server;
	var <bufferDict, numChannels, <gridPoints, <irBuffers, <bufSizes, <delay;
	var <irbuffers, <irspectrums, <rirPath, order;

	classvar <>rirDir;

	*initClass {
		rirDir = File.realpath(HoaRIR.filenameSymbol.asString.dirname.dirname ++ "/Impulse_Responses");
	}

	*new { |type = 'GreatHall', fftSize = 2048, reflection = true, server|
		^super.newCopyArgs(type, fftSize, reflection, server).init
	}

	init {
		rirPath = (type == 'Octagon').if({
			PathName(rirDir +/+ "Octagon");
		}, {
			PathName(rirDir +/+ "GreatHall")
		});

		rirPath = reflection.if({
			rirPath +/+ PathName("HOA3_ACN_N3D_NFDist/Reflection_48000/")
		}, {
			rirPath +/+ PathName("HOA3_ACN_N3D_NFDist/RAW_48000/")
		});

		// set part delay size
		server = server ?? { Server.default };
		delay = fftSize / 2 - server.options.blockSize;

		order = 3;

		numChannels = order.asHoaOrder.size;

		irbuffers = rirPath.files.collect{ |pathname|
			numChannels.collect{arg i; CtkBuffer(pathname.fullPath, channels: i)}
		};

		bufSizes = irbuffers.collect{ |pointBuffers|
			pointBuffers.collect{ |irbuffer|
				PartConv.calcBufSize(fftSize, irbuffer)
			};
		};

		irspectrums = bufSizes.collect{ |pointSizes|
			pointSizes.collect{ |bufSize|
				CtkBuffer.buffer(bufSize, 1)
			}
		};

		irspectrums.do{ |pointspectrums, i|
			pointspectrums.do{ |irspectrum, k|
				irspectrum.preparePartConv(0.0, irbuffers[i][k], fftSize);
			}
		};


	}

	addTo { |score|

		irspectrums.do{ |pointBuffers|
			pointBuffers.do{ |buffer|
				buffer.addTo(score)
			}
		};

		irbuffers.do{ |pointBuffers|
			pointBuffers.do{ |buffer|
				buffer.addTo(score)
			}
		}
	}
}